%% plottrachea
% visualize the data prepared by tracheacollapsibility
% show the tracheal volumes and plot the area vs. length

%% clean slate. FIXME make this a function...
clf
clear all

%% load data and prefs
if ~ispref('tracheav','datadir') % check to see if we have a data dir pref saved
    addpref('tracheav','datadir','.')
end
load('tracheadata21oct2013_2.mat'); % cached preproc data, load from working dir, FIXME ?
pathname = getpref('tracheav','datadir');
[filename, pathname] = uigetfile('*.mat','Choose a patient.',pathname); % what happens if pathname not exist? FIXME
setpref('tracheav','datadir',pathname); % save last used data dir
figure(1)
r = 2;
c = 2;
C = strsplit(filename,'_');
Pcode = C{1};
% temp = strfind(Pname,Pcode);
% temp2 = not(cellfun('isempty',temp));
% Pn = find(temp2(:,1));
[~,Pn]=ismember(Pcode,Pname);
disp(['loading ' Pname{Pn,1}]) 

% already defined in data file:
% respstates = {'EXP','INSP'};
% EXP = 1;
% INSP = 2;


%% display some stats


%% 3D plot of INSP
load([pathname Pcode '_INSP.mat'])
x=coordinates(1:end,1:5:end);
y=coordinates(1:end,2:5:end);
z=coordinates(1:end,3:5:end);
z=z-min(z(:)); % set bottom to zero
zmax = max(z(:));
% zmin = min(z(:));

% generate a 3D figure of trachea in inspiration based on area
s1 = subplot(r,c,1);
% plot3(RoiCenterX, RoiCenterY, RoiCenterZ); axis equal;hold on
% plot3(RoiCenterX, RoiCenterY, Pz{Pn,INSP}); axis equal;hold on
plot3(RoiCenterX, RoiCenterY, t_space{Pn,INSP}); axis equal;hold on
% surf(x,y,z,repmat(Area,1,size(z,2)),'FaceLighting','phong','EdgeColor','none','FaceAlpha',0.7); axis equal
surf(x,y,z,repmat(Area,1,size(z,2)),'FaceLighting','phong','EdgeColor','none','FaceAlpha',0.7); axis equal

colormap('copper')
colorbar
title([Pcode ' INSP ' num2str(Plength(Pn,INSP)) ' slices'])

%% calculate tortuosity for INSP

%% 3D plot of expiration
load([pathname Pcode '_EXP.mat'])
x=coordinates(1:end,1:5:end);
y=coordinates(1:end,2:5:end);
z=coordinates(1:end,3:5:end);
z=z-min(z(:)); % set bottom to zero
zmax = max(max(z(:)),zmax);
% zmin = min(min(z(:)),zmin);

% generate a 3D figure of trachea in expiration based on area
s2 = subplot(r,c,2);
% plot3(RoiCenterX, RoiCenterY, RoiCenterZ); axis equal;hold on
% plot3(RoiCenterX, RoiCenterY, Pz{Pn,EXP}); axis equal;hold on
plot3(RoiCenterX, RoiCenterY, t_space{Pn,EXP}); axis equal;hold on
surf(x,y,z,repmat(Area,1,size(z,2)),'FaceLighting','phong','EdgeColor','none', 'FaceAlpha',0.7); axis equal
colormap('copper')
colorbar
title([Pcode ' EXP ' num2str(Plength(Pn,EXP)) ' slices'])

%% calculate tortuosity for EXP

%% scale plots
% axis([s1,s2],'equal')
zlim(s1,[0 zmax])
zlim(s2,[0 zmax])


%% line plot of area
subplot(r,c,3)
line(t_space{Pn,EXP}, Parea_n{Pn,EXP}) % expiration state
text(t_space{Pn,EXP}(end),Parea{Pn,1}(end),[Pname{Pn,EXP} ' ' thk_str{Pn}])
line(t_space{Pn,INSP}, Parea{Pn,INSP}, 'Color','black') % inspiration state all the data
line(t_space{Pn,EXP}, Parea_n{Pn,INSP}, 'Color','red') % inspiration state matched with expiration

%% scatter plot of volumes for each patient
subplot(r,c,4)
if isa(Vol,'cell')
    Vol = cell2mat(Vol);
end
plot(Vol(:,1),Vol(:,2),'.',Vol(Pn,1),Vol(Pn,2),'or')

line([0 200],[0 200])
line([0 200],[0 300], 'Color','red') % > 50% collapse above this line
% for Pidx = 1:length(patients)
%     text(Vol(Pidx,1),Vol(Pidx,2),Pname{Pidx})
% end
text(Vol(Pn,1),Vol(Pn,2),[' \color{red}' Pname{Pn}])