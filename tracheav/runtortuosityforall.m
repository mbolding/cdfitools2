%Import the data
[~, ~, patients] = xlsread('patient ID.xls','A1:GS1');
respstates = {'EXP','INSP'};

%generate txt file of results
fprintf('%s         \t%s   \t%s   \t%s   \t%s\n','patient','chord','curve','tort1','tort2');  

for patient = patients           
    for respstate = respstates
        filename = [patient{1} '_' respstate{1} '.mat'];
        [chord,curve,tort1,tort2] = tracheatortuosity(filename);
          fprintf('%s\t%f\t%f\t%f\t%f\n',filename,chord,curve,tort1,tort2); 
    end
end