%% trachea collapse measurements 
% Sep 2013
% version 1: normalize inspiration to expiration length to account for
% trachea shortening
% 08 Oct 2013
% version 2: shortening is not physiologically realistic? Instead we will
% shift the area and ROI perimeter data to account for trachea rising and
% falling by using the bottom cut (1cm above carina) as the origin.

%% read patients ID
if ~exist('patient','var')
    disp('reading patient data')
    [~, ~, patients] = xlsread('patient ID.xls', 'A1:GS1');
    disp('done loading.')
else
    disp('using preloaded data.')
end
respstates = {'EXP','INSP'};
EXP = 1;
INSP = 2;

%% calculate the length of trachea
fprintf('%s      \t%s\n','patient','trachea length'); 
Pn=0;
for patient = patients
          Pn=Pn+1;
          Pstate=0;
    for respstate = respstates
        Pstate=Pstate+1;
        filename = [patient{1} '_' respstate{1} '.mat'];
        load(filename);
        Pname{Pn,Pstate} = patient{1};   %#ok<SAGROW>
        Plength(Pn,Pstate) = length(Area);  %#ok<SAGROW>
        Parea{Pn,Pstate} = Area; %#ok<SAGROW>
        Pz{Pn,Pstate} = RoiCenterZ - RoiCenterZ(1); %#ok<SAGROW>
        Pthk(Pn,Pstate) = max(diff(RoiCenterZ)); %#ok<SAGROW>; calculate the maximum thickness of cross sections
        fprintf('%s\t%f\n',filename,Plength(Pn,Pstate));
    end
end

clf
c = 10; % plot columns
r = ceil(Pn/c); % plot rows

%% plot the line of areas along the trachea
% subplot(r,c,1)
% for Pn = 1:length(patients)
%     line(Pz{Pn,EXP},Parea{Pn,1},'Marker','+')
%     text(Pz{Pn,EXP}(end),Parea{Pn,1}(end),Pname{Pn,1})
%     line(Pz{Pn,INSP},Parea{Pn,2},'Color','red','Marker','x')
%     text(Pz{Pn,INSP}(end),Parea{Pn,2}(end),Pname{Pn,1})
% end

%% plot the line of areas along the normalized trachea
% zero is the origin 1cm above carina
figure(1)
for Pn = 1:length(patients) % for each patient
    subplot(r,c,Pn)
    Lp_e = length(Parea{Pn,EXP}); % num slices of trachea in expiration state
    Lp_i = length(Parea{Pn,INSP}); % num slices of trachea in inspiration state
    if Lp_e*Pthk(Pn,EXP) < Lp_i*Pthk(Pn,INSP)
        thk_str = '';
        t_space{Pn,EXP} = (1:Lp_e) * Pthk(Pn,EXP); % vector of slice locations in expiration
        t_space{Pn,INSP} = (1:Lp_i) * Pthk(Pn,INSP); % vector of slice locations in inspiration
        Parea_n{Pn,EXP} = Parea{Pn,EXP}; %#ok<SAGROW>
        if Pthk(Pn,EXP) ~= Pthk(Pn,INSP) % if slice thicknesses differ then resample inspiration at expiration locations
            thk_str = 'diff thk'; % flag for graph
            Parea_n{Pn,INSP} = interp1(t_space{Pn,INSP},Parea{Pn,INSP},t_space{Pn,EXP})'; %#ok<SAGROW>
        else
            Parea_n{Pn,INSP} = Parea{Pn,INSP}(1:Lp_e); %#ok<SAGROW>
        end
        line(t_space{Pn,EXP}, Parea_n{Pn,EXP}) % expiration state
        text(t_space{Pn,EXP}(end),Parea{Pn,1}(end),[Pname{Pn,EXP} ' ' thk_str])
        line(t_space{Pn,INSP}, Parea{Pn,INSP}, 'Color','black') % inspiration state all the data
        line(t_space{Pn,EXP}, Parea_n{Pn,INSP}, 'Color','red') % inspiration state matched with expiration
        xlim([0 80])
        ylim([0 5])
    else
        err_str = ['length data not consistent. Inspiration < expiration length: ' , Pname{Pn,1}];
        error(err_str)
    end
end

%% plot and calculate ratios
figure(2)
for Pn = 1:length(patients) % for each patient
        figure(2)
        Pratio{Pn} = Parea_n{Pn,EXP} ./ Parea_n{Pn,INSP}; %#ok<SAGROW>
end
n_space = linspace(0,100,100); % map everything into percents of length
Pratio_n = zeros(Pn,100);
for Pn = 1:length(patients)
    Lp = length(Pratio{Pn});
    r_space = (1:Lp)/Lp*100;
    Pratio_n(Pn,:) = interp1(r_space,Pratio{Pn},n_space);
    line(n_space, Pratio_n(Pn,:))
    text(100,Pratio_n(Pn,end),Pname{Pn,EXP})
end
hold on
% plot(Pratio_n)
% ylim([0 1])


% area diff with 10 segments

Pratio_ba = squeeze(nanmean(reshape(Pratio_n,[10,10,201]),1)); % binned and averaged into 10% lengths
plot(5:10:95, squeeze(nanmean(Pratio_ba,2)),'Color','red', 'Marker','o')
ylim([0 1])


