clf
clear all
load('14477Z_INSP.mat')

x=coordinates(1:end,1:5:end);
y=coordinates(1:end,2:5:end);
z=coordinates(1:end,3:5:end);

% generate a 3D figure of trachea in inspiration based on area
subplot(1,2,1);
plot3(RoiCenterX, RoiCenterY, RoiCenterZ); axis equal;hold on
surf(x,y,z,repmat(Area,1,size(z,2)),'FaceLighting','phong','EdgeColor','none','FaceAlpha',0.7); axis equal
colormap('copper')
colorbar

clear all
load('14477Z_EXP.mat')

x=coordinates(1:end,1:5:end);
y=coordinates(1:end,2:5:end);
z=coordinates(1:end,3:5:end);

% generate a 3D figure of trachea in expiration based on area
subplot(1,2,2)
plot3(RoiCenterX, RoiCenterY, RoiCenterZ); axis equal;hold on
surf(x,y,z,repmat(Area,1,size(z,2)),'FaceLighting','phong','EdgeColor','none', 'FaceAlpha',0.7); axis equal
colormap('copper')
colorbar