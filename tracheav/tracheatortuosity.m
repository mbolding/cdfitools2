function [chord,curve,tort1,tort2] = tracheatortuosity(filename) 
% calculate the chord of centerline, length of centerline, and tortuosity
% of the lumen
 
load(filename)

% calculate chord as shortest distance from start to end of centerline
chord=sqrt((RoiCenterX(1)-RoiCenterX(end))^2+(RoiCenterY(1)-RoiCenterY(end))^2+(RoiCenterZ(1)-RoiCenterZ(end))^2);

% calculate point to point vectors along centerline
dx = diff(RoiCenterX);
dy = diff(RoiCenterY);
dz = diff(RoiCenterZ);
curve=sum(sqrt(dx.^2+dy.^2+dz.^2));

% calculate tortuosity index 1
tort1 = curve/chord;

% calculate tortuosity index 2; refer to "Surface tortuosity and its
% application to analyzing cracks in concrete. Tong Zhang, George Nagy.
% Submitted to International Conference on Pattern Recognition, 2004.
V = [dx, dy, dz]; % vectors between points
m = sum(V.^2,2).^0.5; % magnitudes of the vectors
v = V./[m,m,m]; % normalized vectors
k = 2:length(v); % vector indexes
n = cross(v(k-1,:),v(k,:)); % normals
d = dot(v(k-1,:),v(k,:),2); % dot products
rho = acos(d); % inplane angle between vectors
k = 2:length(n); 
tau = acos(dot(n(k-1,:),n(k,:),2)); % tortional angle between vectors
k = 1:length(tau);
theta = sqrt( rho(1:k).^2 + tau.^2  ); % total angle between vectors
tort2 = sum(theta)/length(Area); % sum of the total angles






